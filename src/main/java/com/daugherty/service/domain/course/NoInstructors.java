package com.daugherty.service.domain.course;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

/**
 * @author garethdavies
 *
 */
@Projection(name = "noInstructors", types = Course.class)
public interface NoInstructors {

  String getName();

  String getFormat();

  String getLocation();

  @Value("#{target.name} (#{target.format})")
  String getNameAndFormat();

}
