package com.daugherty.service.domain.course;

import org.springframework.data.mongodb.repository.MongoRepository;;

/**
 * @author garethdavies
 *
 */
public interface CourseRepository extends MongoRepository<Course, String> {

}
