package com.daugherty.service.domain.course;

import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CourseDomainServiceApplicationTests {

  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private CourseRepository repository;

  @Test
  public void contextLoads() {
    assertThat(repository, is(notNullValue()));
  }

  @Test
  public void getCourses() throws Exception {
    Long initialCount = repository.count();

    MockHttpServletRequestBuilder request = get("/courses");

    mockMvc.perform(request)
        .andExpect(status().isOk())
        .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
        .andExpect(jsonPath("$._embedded.courses", hasSize(initialCount.intValue())))
        .andExpect(jsonPath("$._links.self.href", endsWith("/courses")))
        .andExpect(jsonPath("$.page.size", is(20)));
  }

}
